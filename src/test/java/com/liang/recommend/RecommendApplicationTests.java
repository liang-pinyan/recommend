package com.liang.recommend;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liang.recommend.algorithm.Item;
import com.liang.recommend.algorithm.Latent_Factor;

import com.liang.recommend.manager.AutoManager;
import com.liang.recommend.manager.ResetData;
import com.liang.recommend.mapper.UserActionMapper;
import com.liang.recommend.pojo.Entity;
import com.liang.recommend.redis.MyRedis.RecentActions;
import com.liang.recommend.service.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class RecommendApplicationTests {
    @Autowired
    UserLikeService userLikeService;
    @Autowired
    UserActionService userActionService;

    @Autowired
    MovieTypeService movieTypeService;

    @Autowired
    AutoManager autoManager;
    @Autowired
    ResetData resetData;
    @Autowired
    MovieService movieService;
    @Autowired
    RotationService rotationService;
    @Autowired
    UserActionMapper userActionMapper;
    @Autowired
    MovieCoordinationService movieCoordinationService;
    @Autowired
    RecentActions recentActions;
    @Autowired
    ManagerService managerService;

    @Test
    void contextLoads() {

//================================管理员操作==============================//
//           autoManager.InitData();//初始化数据（自带格式化）
//         autoManager.updateTask();//更新协同表

//        resetData.toResetData(); //格式化用户行为数据，基本用不到！

//=========================关键==========================================
//        /*
//         * *===========================================*
//         * *        电影1   电影2   电影3  电影4       *
//         * * 用户1   4.2     5.0     null   null       *
//         * * 用户2   2.3     null     5.0    0.0       *
//         * * 用户3   4.7     null     null   5.0       *
//         * *===========================================*
//         */
//        Latent_Factor latent_factor = new Latent_Factor();
//        Double[][] realR ={
//                {4.2,5.0,0.0,0.0},
//                {2.3,0.0,5.0,0.0},
//                {4.7,0.0,0.0,5.0}
//        };
//         /*
//          * *===========================================*
//          *   10分   剧情   爱情   动漫  科幻  历史     *
//          * * 用户1  5.44   2.5     0.0   0.0   0.0     *  value = i+= 评分*标签值
//          * * 用户2  1.61   0.68    2.5   0.0   2.5     *
//          * * 用户3  6.79   2.91    0.0   0.0   0.0     *
//          * *===========================================*
//         */
//        Double[][] q ={
//                {5.44,2.5,0.0,0.0,0.0},
//                {1.61,0.68,2.5,0.0,2.5},
//                {6.79,2.91,0.0,0.0,0.0}
//        };
//        /*
//         * *========================================*
//         * *        电影1   电影2   电影3  电影4    *
//         * * 剧情    0.7      0.5           0.7     *
//         * * 爱情    0.3                    0.3     *
//         * * 动漫     0              0.5            *
//         * * 科幻     0       0.5                   *
//         * * 历史     0              0.5            *
//         * *========================================*
//         */
//        Double[][] p ={
//                {0.7,0.5,0.0,0.7},
//                {0.3,0.0,0.5,0.3},
//                {0.0,0.0,0.5,0.0},
//                {0.0,0.5,0.0,0.0},
//                {0.0,0.0,0.0,0.0},
//        };
//        Double[][] r = latent_factor.getR(q, p);
//        latent_factor.toPrint(q);
//        System.out.println("================");
//        latent_factor.toPrint(p);
//        System.out.println("================");
//        latent_factor.toPrint(r);
//        System.out.println("================");
//        Double lossFunction = latent_factor.lossFunction(realR, r);
//        System.out.println(lossFunction);
//=============================================================================================//

//        List<Movie> recommend = movieService.getRecommend(1L, 1);
//        for (Movie movie : recommend) {
//            System.out.println(movie);
//        }
//=============================================================================================//
//        List<Rotation> recommend = rotationService.getRecommend(8L, 1);
//        for (Rotation rotation : recommend) {
//            System.out.println(rotation.toString());
//        }
//=================================测试service中的getRealR方法=========================================//
//        Double[][] realR = userActionService.getRealR();
//        for (Double[] doubles : realR) {
//            for (Double aDouble : doubles) {
//                System.out.print(aDouble+"\t");
//            }
//            System.out.println("");
//        }
//=================================测试Item方法=========================================//
//        Item item = new Item();
//        HashMap<Long, List<Long>> map = new HashMap<>();
//        List<Long> list1 = new ArrayList<>();
//        list1.add(1L);
//        list1.add(2L);
//        list1.add(4L);
//        map.put(1L,list1);
//        List<Long> list2 = new ArrayList<>();
//        list2.add(2L);
//        list2.add(3L);
//        list2.add(5L);
//        map.put(2L,list2);
//        List<Long> list3 = new ArrayList<>();
//        list3.add(3L);
//        list3.add(4L);
//        map.put(3L,list3);
//        List<Long> list4 = new ArrayList<>();
//        list4.add(2L);
//        list4.add(3L);
//        list4.add(4L);
//        map.put(4L,list4);
//        List<Long> list5 = new ArrayList<>();
//        list5.add(1L);
//        list5.add(4L);
//        map.put(5L,list5);
//        List<Long> list6 = new ArrayList<>(); //user
//        list6.add(1L);
//        list6.add(2L);
//        list6.add(3L);
//        list6.add(4L);
//        list6.add(5L);
//        List<Long> list7 = new ArrayList<>(); //movie
//        list7.add(1L);
//        list7.add(2L);
//        list7.add(3L);
//        list7.add(4L);
//        list7.add(5L);
//        Entity entity = item.stepTwo(map, list6, list7);
//        List<Long> movieId = entity.getMovieId();
//        for (Long aLong : movieId) {
//            System.out.print(aLong+"\t");
//        }
//        System.out.println("");
//        Integer[][] matrix = entity.getMatrix();
//        System.out.println("=========================");
//        for (Integer[] integers : matrix) {
//            for (Integer integer : integers) {
//                System.out.print(integer+"\t");
//            }
//            System.out.println();
//        }
//=================================运行Item方法=========================================//
//        Item item = new Item();
//        Map<Long, List<Long>> map = item.stepOne(userActionService.getAllUserId(), userActionService.getUserActionCollection());
//        Entity entity = item.stepTwo(map, userActionService.getAllUserId(), userActionService.getAllUserMovieId());
//        List<Long> movieId = entity.getMovieId();
//        for (Long aLong : movieId) {
//            System.out.print(aLong+"\t");
//        }
//        System.out.println("");
//        Integer[][] matrix = entity.getMatrix();
//        System.out.println("=========================");
//        for (Integer[] integers : matrix) {
//            for (Integer integer : integers) {
//                System.out.print(integer+"\t");
//            }
//            System.out.println("");
//            System.out.println("===========================");
//        }
//=============================更新movie_coordination的==========================================//
//          managerService.deleteMovieCoordination();  //清空movie_coordination表
//           movieCoordinationService.Insert();  //插入movie_coordination表 主要依据userAction的数据

    }

}
