import com.baomidou.mybatisplus.annotation.DbType;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;



public class MyCode {
    public static void main(String[] args) {
        //代码自动生成器对象
        AutoGenerator generator = new AutoGenerator();
        //配置策略
        //1. 全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        //D:\XL-BanGong\XL-IntelliJ IDEA\IntelliJ IDEA 2020.1.3\workplace\springboot-10-test
        String path = System.getProperty("user.dir");//获取当前项目路径
        globalConfig.setOutputDir(path+"/src/main/java");//设置输出路径
        globalConfig.setAuthor("小梁");//设置作者
        globalConfig.setOpen(false);//是否打开资源目录
        globalConfig.setFileOverride(false);//是否覆盖已有
        globalConfig.setServiceName("%sService");//去Service的I前缀
        globalConfig.setIdType(IdType.ID_WORKER);//设置主键id策略
        globalConfig.setDateType(DateType.ONLY_DATE);//设置时间格式
        globalConfig.setSwagger2(true);//是否使用swagger2
        generator.setGlobalConfig(globalConfig);
        //==========================================================
        //2.  设置数据源
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://47.111.28.6:3306/recommenddemo01?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC&useSSL=false");
        dsc.setUsername("test");
        dsc.setPassword("123456");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setDbType(DbType.MYSQL);
        generator.setDataSource(dsc);
        //==========================================================
        //3. 包配置
        PackageConfig pc = new PackageConfig();
//        pc.setModuleName("utils");//类似于项目名
        pc.setParent("com.liang.recommend");//项目名的对上包
        pc.setEntity("pojo");//实体类名字
        pc.setMapper("mapper");//mapper名字
        pc.setController("controller");//controller名字
        generator.setPackageInfo(pc);
        //==========================================================
        //4. 策略配置
        StrategyConfig config = new StrategyConfig();
        config.setInclude("rotation");//设置要映射的表名
        config.setNaming(NamingStrategy.underline_to_camel);//下划线转驼峰
        config.setColumnNaming(NamingStrategy.underline_to_camel);
        config.setEntityLombokModel(true);//是否开启lombok
        config.setRestControllerStyle(true);//是否是restController风格
        config.setControllerMappingHyphenStyle(true);//请求风格：localhost:8080/hello_id_2
//        config.setLogicDeleteFieldName("deleted");//逻辑删除
        //5. 自动填充策略
//        TableFill createTime = new TableFill("create_time", FieldFill.INSERT);//
//        TableFill updateTime = new TableFill("update_time", FieldFill.INSERT_UPDATE);
//        ArrayList<TableFill> list = new ArrayList<>();
//        list.add(createTime);
//        list.add(updateTime);
//        config.setTableFillList(list);
        //6. 乐观锁
//        config.setVersionFieldName("version");//设置version字段为乐观锁
        generator.setStrategy(config);
        //==========================================================
        generator.execute();//执行自动生成
    }
}