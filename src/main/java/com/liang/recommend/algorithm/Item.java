package com.liang.recommend.algorithm;

import com.liang.recommend.pojo.Entity;
import com.liang.recommend.pojo.UserAction;

import java.util.*;

public class Item {

    /**
     * 测试窗口
     * @param args
     */
    public static void main(String[] args) {
        forTest();
    }

    /**
     * 用于测试的数据
     */
    public static void forTest(){
        Item item = new Item();
        HashMap<Long, List<Long>> map = new HashMap<>();
        List<Long> list1 = new ArrayList<>();
        list1.add(1L);
        list1.add(2L);
        list1.add(4L);
        map.put(1L,list1);
        List<Long> list2 = new ArrayList<>();
        list2.add(2L);
        list2.add(3L);
        list2.add(5L);
        map.put(2L,list2);
        List<Long> list3 = new ArrayList<>();
        list3.add(3L);
        list3.add(4L);
        map.put(3L,list3);
        List<Long> list4 = new ArrayList<>();
        list4.add(2L);
        list4.add(3L);
        list4.add(4L);
        map.put(4L,list4);
        List<Long> list5 = new ArrayList<>();
        list5.add(1L);
        list5.add(4L);
        map.put(5L,list5);
        List<Long> list6 = new ArrayList<>(); //user
        list6.add(1L);
        list6.add(2L);
        list6.add(3L);
        list6.add(4L);
        list6.add(5L);
        List<Long> list7 = new ArrayList<>(); //movie
        list7.add(1L);
        list7.add(2L);
        list7.add(3L);
        list7.add(4L);
        list7.add(5L);
        Entity entity = item.stepTwo(map, list6, list7);
        List<Long> movieId = entity.getMovieId();
        for (Long aLong : movieId) {
            System.out.print(aLong+"\t");
        }
        System.out.println("");
        Integer[][] matrix = entity.getMatrix();
        System.out.println("=========================");
        for (Integer[] integers : matrix) {
            for (Integer integer : integers) {
                System.out.print(integer+"\t");
            }
            System.out.println();
        }
    }

    /**
     * 步骤1：
     * @param userList
     * @return
     */
    public Map<Long, List<Long>> stepOne(List<Long> userList,List<UserAction> allAction){
        Map<Long, List<Long>> map = new HashMap<>(); //key=用户id list=该用户喜欢的电影id的集合
       //2.6=1+2*(4/5) 即评分4分及以上才算喜欢的电影
        for (Long id : userList) {
            List<Long> list = new ArrayList<>();
            for (UserAction action : allAction) {
                if (action.getUserId()==id&&action.getScore()>=2.6){//找到喜欢该电影的所有用户id
                    list.add(action.getMovieId());
                }
            }
            map.put(id,list);
        }
        return map;
    }

    /**
     *步骤2：
     * @param map
     * @param userList
     * @param movieId
     * @return
     */
    public Entity stepTwo(Map<Long, List<Long>> map, List<Long> userList, List<Long> movieId){//均已经排序，一一对应
        if (map.size()==userList.size()){
            Entity entity = new Entity();
            Integer[][] matrix = new Integer[movieId.size()][movieId.size()];
            for (int i = 0; i < movieId.size(); i++) {
                Arrays.fill(matrix[i],0);
            }
            HashMap<Long, Integer> idMap = new HashMap<>();
            int i=0;
            for (Long aLong : movieId) {
                idMap.put(aLong,i);//获得一个id在查询结果List中的序号
                i+=1;
            }
            for (Long id : userList) {
                List<Long> list = map.get(id);
                for (Long x : list) {
                    for (Long y : list) {
                        if (y!=x){
                            matrix[idMap.get(x)][idMap.get(y)]+=1;
                        }
                    }
                }
            }
            entity.setMovieId(movieId);
            entity.setMatrix(matrix);
            return entity;

        }else {
            System.out.println("参数长度不一致无法处理");
            return new Entity(null,null);
        }
    }

}

