package com.liang.recommend.algorithm;

import com.liang.recommend.pojo.UserLike;

//欧几里得距离：
public class Euclidean_metric {

    public static void main(String[] args) {
        UserLike user1 = new UserLike(1L,21.874F,2.9744F,0F,8.3856F,8.5512F,5.7072F,0F,0F,0F,2.6164F,0F,0F,0F,0F,2.8376F,2.8376F,0F,5.6836F,5.6596F,2.7668F,0F);
        UserLike user2 = new UserLike(2L,11.328F,0F,2.906F,2.7148F,0F,5.5736F,0F,0F,0F,5.6948F,2.7148F,0F,0F,0F,0F,0F,0F,5.5736F,2.8428F,0F,0F);

        Euclidean_metric metric = new Euclidean_metric();
        double v = metric.euclidean_dis(user1, user2);
        System.out.println(v);

    }
    /**
     *
     * @param user1
     * @param user2
     * @return double //user1和user2的欧式距离
     */
    public double euclidean_dis(UserLike user1,UserLike user2){
        //user1的轴值
        float x=0;
        //user2的轴值
        float y=0;
        //x和y的差值
        float x_y=0;
        //x和y的差值的平方的和
        double sum=0;
        //遍历不同的轴，共有21和属性即21个轴
        for (int i =1;i<=21;i++){
           x = user1.getMethod(i);
           y = user2.getMethod(i);
           x_y=x-y;
           sum =sum + Math.pow(x_y,2);
        }
        return sum;
    }
}
