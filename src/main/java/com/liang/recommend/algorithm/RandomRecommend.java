package com.liang.recommend.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomRecommend {

    public static void main(String[] args) {
        for (int i = 0; i < 200; i++) {
            System.out.println(getRandomGrade());
        }

    }


    public List<Integer> getRandom(){
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int random = (int)(Math.random()*90);
            list.add(random);
        }
        for (int i = 0; i < list.size(); i++) {
            Integer a = list.get(i);
            for (int j = 0; i < list.size(); j++) {
                if (i == j) {
                    break;
                }
                if (a==list.get(j)) {
                    list.remove(i);
                }
            }
        }
        return list;
    }


    public static float getRandomGrade(){
        Random random = new Random();
        float v = (random.nextInt(3)+2)+random.nextFloat();
        return v;
    }
}
