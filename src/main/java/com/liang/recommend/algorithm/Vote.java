package com.liang.recommend.algorithm;

import org.springframework.stereotype.Component;

import java.util.*;

/**权重票选
 * @author xiao
 */
@Component
public class Vote {

    /**
     * 测试窗口
     * @param args
     */
    public static void main(String[] args) {
//        forTest();
    }

    /**
     * 测试
     */
    public static void forTest(){
        HashMap<Integer, Integer> map = new HashMap<>();

        List<Long> list1 = new ArrayList<>();
        list1.add(1L);
        list1.add(2L);
        list1.add(3L);

        List<Long> list2 = new ArrayList<>();
        list2.add(2L);
        list2.add(3L);
        list2.add(4L);
        list2.add(5L);

        List<Long> list3 = new ArrayList<>();
        list3.add(3L);
        list3.add(5L);
        list3.add(6L);
        Vote vote = new Vote();
        List<Long> data = vote.voteResult(list1, list2, list3);
        System.out.println(data.toString());
    }

    /**
     * 权重处理方法
     * @param A 权重3
     * @param B 权重4
     * @param C 权重5
     * @author xiao
     * @return 权重排行后的电影id
     */
    public List<Long> voteResult(List<Long> A,List<Long> B,List<Long> C){
        HashMap<Long, Integer> map = new HashMap<>();
        for (Long a : A) {
            map.put(a,3);
        }
        for (Long b : B) {
            if (map.containsKey(b)) {
                map.put(b, map.get(b) + 4);
            } else {
                map.put(b, 4);
            }
        }
        for (Long c : C) {
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 5);
            } else {
                map.put(c, 5);
            }
        }
        HashSet<Integer> set = new HashSet<>();

        for (Long id : map.keySet()) {
            set.add(map.get(id));
        }
        List<Integer> data = new ArrayList<>();
        for (Integer num : set) {
            data.add(num);
        }
        Collections.sort(data);
        Collections.reverse(data);

        return getKeyList(map, data);
    }

    /**
     * 得到权重由高到低排行的电影id集合
     * @param map
     * @param data
     * @return
     */
    public static List<Long> getKeyList(HashMap<Long, Integer> map,List<Integer> data) {
        List<Long> keyList = new ArrayList();
        for (Integer num : data) {
            for (Long key : map.keySet()) {
                if (map.get(key).equals(num)){
                    keyList.add(key);
                }
            }
        }
        System.out.println(keyList.toString());
        return keyList;
    }





}
