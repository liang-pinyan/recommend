package com.liang.recommend.manager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.liang.recommend.controller.UserActionController;
import com.liang.recommend.controller.UserLikeController;
import com.liang.recommend.pojo.Action;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import jxl.*;



/*
插入原始数据的工具类
 */
@Component
public class InsertData {
    @Autowired
    UserLikeController userLikeController;
    @Autowired
    UserActionController userActionController;

    Sheet sheet;
    Workbook book;
    Cell cell;

    /**
     * 编程调试窗口
     * @param args
     */
    public static void main(String[] args) {
//        InsertData insertData = new InsertData();
//        List<Action> xlsData = insertData.getXLSData();
//        for (Action xlsDatum : xlsData) {
//            System.out.println(xlsDatum.toString());
//        }
    }

    /**
     * 自动化插入数据
     */
    public void AutoInsert(){
        Integer[] userIdData = getJsonData();
        for (Integer user : userIdData) {
            userLikeController.register(user);
        }

        List<Action> userActionData = getXLSData();
        for (Action view : userActionData) {
            userActionController.view(view.getUserId(),view.getMovieId());
        }
        for (Action action : userActionData) {
            userActionController.action(action.getUserId(),action.getMovieId(),action.getGrade(),action.getCollection());
        }

    }

    /**
     * 读取xls文件
     */
    public  List<Action> getXLSData(){
        List<Action> actions = new ArrayList<>();
        Action action;
        try {
            book = Workbook.getWorkbook(new File("src/main/resources/data/data.xls"));
            sheet = book.getSheet(0); //第一个工作簿
            int i=1; //行，因为第一行是标题
            while(true)
            {
                action = new Action();
                for (int j = 0; j < 4; j++) {
                    cell=sheet.getCell(j,i); //（列，行）
                    if ("".equals(cell.getContents())){ //如果读取的数据为空
                        break;
                    }else {
                        if(j==0){
                            action.setUserId(Integer.parseInt(cell.getContents()));
                        }else if(j==1){
                            action.setMovieId(Integer.parseInt(cell.getContents()));
                        }else if(j==2){
                            action.setGrade(Float.parseFloat(cell.getContents()));
                        }else{
                            action.setCollection(Float.parseFloat(cell.getContents()));
                        }

                    }
                }
                actions.add(action);
                i+=1;
            }
        } catch (Exception e) {
            System.out.println("正常错误，不影响");
        }finally {
            book.close();
        }
        return actions;
    }

    /**
     * 得到id数组
     */
    public static Integer[] getJsonData(){
        String s = readJsonFile("src/main/resources/data/register.json");
        JSONObject object = JSON.parseObject(s);
        JSONArray data = object.getJSONArray("DATA");//构建数组
        Integer[] list = new Integer[data.size()];
        for (int i = 0; i < data.size(); i++) {
            list[i] = Integer.parseInt((String) data.get(i));
        }
        return list;
    }

    /**
     * 读取json文件
     * @param path
     * @return
     */
    public static String readJsonFile(String path) {
        String jsonStr = "";
        try {
            File jsonFile = new File(path);
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile),"utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
