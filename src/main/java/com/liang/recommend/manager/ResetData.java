package com.liang.recommend.manager;

import com.liang.recommend.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
重置数据的工具类
 */
@Component
public class ResetData {
    @Autowired
    ManagerService managerService;

    public void toResetData(){
        managerService.deleteActionData();
    }

}
