package com.liang.recommend.manager;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liang.recommend.algorithm.Latent_Factor;
import com.liang.recommend.controller.MovieController;
import com.liang.recommend.mapper.UserActionMapper;
import com.liang.recommend.pojo.P;
import com.liang.recommend.pojo.UserAction;
import com.liang.recommend.pojo.UserLike;
import com.liang.recommend.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/*
管理员所有任务的工具类
 */
@Component
public class AutoManager {
    @Autowired
    ManagerService managerService;
    @Autowired
    UserActionMapper userActionMapper;
    @Autowired
    UserCoordination2Service userCoordination2Service;
    @Autowired
    UserCoordinationService userCoordinationService;
    @Autowired
    MovieTypeService movieTypeService;
    @Autowired
    UserLikeService userLikeService;
    @Autowired
    LatentFactorService latentFactorService;
    @Autowired
    MovieService movieService;
    @Autowired
    InsertData insertData;
    @Autowired
    ResetData resetData;
    @Autowired
    MovieCoordinationService movieCoordinationService;


    /**
     * 定时任务
     * 需要定时执行的任务添加到该方法中即可
     *
     * 以添加：任务1.
     *
     */
    @Scheduled(cron = "0 0 0 * * *") //每晚0点更新
    public void Task(){
        updateTask();
    }

    /**
     * 任务 1.
     * 更新三表任务：
     * 管理员根据userAction和userLike表，跟新相关三个推荐表
     */
    public void updateTask(){
        //清空三个库
        managerService.delete1();
        managerService.delete2();
        managerService.delete3();
        managerService.deleteMovieCoordination();
        //得到所有有过行为的用户
        QueryWrapper<UserAction> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("DISTINCT user_id")
                    .orderByAsc("user_id");
        List<UserAction> userActions = userActionMapper.selectList(queryWrapper);

        for (UserAction userAction : userActions) { //逐个用户添加自己与他人的行为距离
            Long id = userAction.getUserId();
            userCoordinationService.toInsertUserCoordination(id);
            userCoordination2Service.toInsertUserCoordination2(id);
        }

        //更新latentFactor表
        List<P> p = movieTypeService.getP();
        List<UserLike> userLikeList = userLikeService.getUserLikeList();
        //得到矩阵
        Latent_Factor latent_factor = new Latent_Factor();
        Double[][] matrixP = latent_factor.getMatrixP(p);
        Double[][] matrixQ = latent_factor.getMatrixQ(userLikeList);
        //得到轴
        Long[] axisP = latent_factor.getAxisP(p);
        Long[] axisQ = latent_factor.getAxisQ(userLikeList);
        //计算矩阵
        Double[][] matrixR = latent_factor.getR(matrixQ, matrixP);
        //打印检查
//        latent_factor.toPrint(matrixP);
//        latent_factor.toPrint(matrixQ);
//        latent_factor.toPrint(matrixR);
        //将矩阵插入数据库
        latentFactorService.toInsertLatentFactor(matrixR,axisQ,axisP);
        movieCoordinationService.Insert();

    }

    /**
     * 任务 2.
     * 重置数据（使用初始化数据集）:
     * 即为格式化userAction和userLike，并重新插入数据
     *
     */
    public void InitData(){
        resetData.toResetData();
        insertData.AutoInsert();

    }



}
