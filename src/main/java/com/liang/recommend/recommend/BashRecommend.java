package com.liang.recommend.recommend;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liang.recommend.mapper.UserActionMapper;
import com.liang.recommend.pojo.Movie;
import com.liang.recommend.pojo.UserAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BashRecommend {

    @Autowired
    UserActionMapper userActionMapper;

    /**
     *不重复推荐处理 得到具体的电影列表
     * @param userId
     * @param movies
     * @return
     */
    public List<Movie> getRecommendMovieList(Long userId, List<Movie> movies){
        //不重复推荐处理
        QueryWrapper<UserAction> wrapper = new QueryWrapper<UserAction>()
                .eq("user_id",userId);
        List<UserAction> list = userActionMapper.selectList(wrapper);
        for (UserAction action : list) {
            Long movieId = action.getMovieId();
            for (Movie movie : movies) {
                Long mymovie = movie.getMId();
                if (mymovie == movieId) {
                    movies.remove(movie);
                    break;
                }
            }
        }
        return movies;
    }

    /**
     *不重复推荐处理 得到具体的电影id的列表
     * @param userId
     * @param moviesIds
     * @return
     */
    public List<Long> getRecommendMovieIdList(Long userId, List<Long> moviesIds){
        //不重复推荐处理
        QueryWrapper<UserAction> wrapper = new QueryWrapper<UserAction>()
                .select("distinct movie_id")
                .eq("user_id",userId);
        List<UserAction> list = userActionMapper.selectList(wrapper);

        for (UserAction action : list) {
            Long movieId = action.getMovieId();
            for (Long id : moviesIds) {
                if (id == movieId) {
                    moviesIds.remove(id);
                    break;
                }
            }
        }
        return moviesIds;
    }
}
