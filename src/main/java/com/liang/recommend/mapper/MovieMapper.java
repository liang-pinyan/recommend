package com.liang.recommend.mapper;

import com.liang.recommend.pojo.Movie;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Repository
public interface MovieMapper extends BaseMapper<Movie> {


}
