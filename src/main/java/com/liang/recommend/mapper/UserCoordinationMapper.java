package com.liang.recommend.mapper;

import com.liang.recommend.pojo.UserCoordination;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Repository
public interface UserCoordinationMapper extends BaseMapper<UserCoordination> {

    @Update("TRUNCATE `user_coordination`")
    void deleteAll();

}
