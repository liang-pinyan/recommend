package com.liang.recommend.mapper;

import com.liang.recommend.pojo.Rotation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小梁
 * @since 2020-12-27
 */
@Repository
public interface RotationMapper extends BaseMapper<Rotation> {

}
