package com.liang.recommend.mapper;

import com.liang.recommend.pojo.MovieLabel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
public interface MovieLabelMapper extends BaseMapper<MovieLabel> {

}
