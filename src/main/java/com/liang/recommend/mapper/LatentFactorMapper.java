package com.liang.recommend.mapper;

import com.liang.recommend.pojo.LatentFactor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小梁
 * @since 2020-12-25
 */
@Repository
public interface LatentFactorMapper extends BaseMapper<LatentFactor> {

    @Update("TRUNCATE `latent_factor`")
    void deleteAll();

}
