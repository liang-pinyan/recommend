package com.liang.recommend.controller;


import com.liang.recommend.pojo.CommonResult;
import com.liang.recommend.service.UserActionService;
import com.liang.recommend.service.UserLikeService;
import com.liang.recommend.util.RedisUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@RestController
@RequestMapping("/user-action")
public class UserActionController {
    @Autowired
    UserActionService userActionService;
    @Autowired
    UserLikeService userLikeService;
    @Autowired
    RedisUtil redisUtil;


    @ApiOperation("用户浏览")
    @GetMapping("/view/{uid}/{mid}")
    public CommonResult view(@PathVariable("uid") int userId, @PathVariable("mid") int movieId){
        int i = userActionService.addUserAction(userId, movieId);
        String key=Integer.toString(userId);
        if (redisUtil.hasKey(key)&&redisUtil.lGetListSize(key)>7){
                redisUtil.rPop(key); //大于7先弹出
                redisUtil.lSet(key,movieId); //再表头插入
        }else {
            redisUtil.lSet(key,movieId);
        }
        if (i>0){  //成功
            return new CommonResult(1,true,null);
        }else {
            return new CommonResult(0,false,null);
        }
    }


    @ApiOperation("用户行为")
    @PostMapping("/action")
//    @Async
    public CommonResult<String> action(@RequestParam int userId, @RequestParam int movieId,@RequestParam Float grade,@RequestParam Float collection){
        int i=0;
        if(grade!=0.0||collection!=0.0){ //至少有行为
            i = userActionService.updateUserAction(userId, movieId, grade, collection);
            if (grade>=4.0||collection!=0){
                int flag = userLikeService.updateUser(userId, movieId,grade,collection);
                if (flag>0){
                    System.out.println("异步执行");
                }else {
                    System.out.println("异步任务失败");
                }
            }
        }
        if (i>0){  //成功
            return new CommonResult(1,true,"成功的行为");
        }else {
            return new CommonResult(0,false,"你还没认真浏览该电影的信息！");
        }
    }

}

