package com.liang.recommend.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liang.recommend.mapper.RotationMapper;
import com.liang.recommend.pojo.CommonResult;
import com.liang.recommend.pojo.Movie;
import com.liang.recommend.pojo.Rotation;
import com.liang.recommend.service.RotationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiao
 * @since 2020-12-09
 */
@RestController
public class RotationController {

    @Autowired
    RotationService rotationService;
    @Autowired
    RotationMapper rotationMapper;


    @CrossOrigin
    @PostMapping("/getRotation")
    public CommonResult getRotation(@RequestParam Long userId, @RequestParam Integer index){
        List<Rotation> recommend = rotationService.getRecommend(userId, index);
        for (Rotation rotation : recommend) {
            rotation.setRotationChart("http://localhost:8080/rotation/"+rotation.getRotationChart());
        }
        if (recommend.size()==5){
            return new CommonResult(1,true,recommend);
        }else {
            return new CommonResult(1,false,"后台推荐不足");
        }
    }

    @PostMapping("/getItemRotation")
    public CommonResult getItemRecommend(@RequestParam Long userId, @RequestParam Integer index){
        List<Long> allMovies = rotationService.getMovieList5(userId);
        List<Long> recommend = allMovies.subList((index - 1) * 5, (index - 1) * 5 + 5); //一页5个结果
        QueryWrapper<Rotation> wrapper = new QueryWrapper<>();
        wrapper.in("movie_id",recommend);
        List<Rotation> movies = rotationMapper.selectList(wrapper);
        if (recommend.size()==5){
            return new CommonResult(1,true,movies);
        }else {
            return new CommonResult(1,false,"后台推荐不足");
        }
    }
}
