package com.liang.recommend.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liang.recommend.mapper.MovieMapper;
import com.liang.recommend.pojo.CommonResult;
import com.liang.recommend.pojo.Movie;
import com.liang.recommend.service.MovieService;
import com.liang.recommend.service.RotationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@RestController
@RequestMapping("/movie")
public class MovieController {

    @Autowired
    MovieService movieService;
    @Autowired
    RotationService rotationService;
    @Autowired
    MovieMapper movieMapper;


    @PostMapping("/getRecommend")
    public CommonResult getRecommend(@RequestParam Long userId, @RequestParam Integer index){
        List<Movie> recommend = movieService.getRecommend(userId, index);
        if (recommend.size()==5){
            return new CommonResult(1,true,recommend);
        }else {
            return new CommonResult(1,false,"后台推荐不足");
        }
    }

    @PostMapping("/getItemRecommend")
    public CommonResult getItemRecommend(@RequestParam Long userId, @RequestParam Integer index){
        List<Long> allMovies = movieService.getMovieList5(userId);
        List<Long> recommend = allMovies.subList((index - 1) * 5, (index - 1) * 5 + 5); //一页5个结果
        List<Movie> movies = new ArrayList<>();
        QueryWrapper<Movie> wrapper = new QueryWrapper<>();
        wrapper.in("m_id",recommend);
        movies = movieMapper.selectList(wrapper);
        if (recommend.size()==5){
            return new CommonResult(1,true,movies);
        }else {

            return new CommonResult(1,false,"后台推荐不足");
        }
    }




}

