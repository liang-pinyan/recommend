package com.liang.recommend.controller;

import com.liang.recommend.pojo.CommonResult;

import com.liang.recommend.service.MovieTypeService;
import com.liang.recommend.service.UserActionService;
import com.liang.recommend.service.UserLikeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;



/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@RestController
@RequestMapping("/user-like")
public class UserLikeController {
    @Autowired
    UserLikeService userLikeService;
    @Autowired
    MovieTypeService movieTypeService;
    @Autowired
    UserActionService userActionService;



    @ApiOperation("注册开辟记录")
    @GetMapping("/register/{id}")
    public CommonResult<String> register(@PathVariable("id") int id){
        int i = userLikeService.addUser(id);
        if (i>0){  //成功
            return new CommonResult(1,true,"成功");
        }else {
            return new CommonResult(0,false,"该用户已存在");
        }
    }


    /**
     * /user-like/regis
     *注册用户Like表
     * @date  16:31
     */
    @PostMapping("/regis")
    public CommonResult regis(@RequestParam Long id) {
        int i = userLikeService.addUser(id.intValue());
        if (i>0){  //成功
            return new CommonResult(1,true,null);
        }else {
            return new CommonResult(0,false,null);
        }
    }



}

