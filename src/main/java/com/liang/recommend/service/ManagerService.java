package com.liang.recommend.service;


public interface ManagerService {

    void delete1();

    void delete2();

    void delete3();

    /**
     * 重置行为
     * 包括：清空user_like表和user_action表
     */
    void deleteActionData();

    /**
     * 重置行为
     * 清空movie_coordination表
     */
    void deleteMovieCoordination();
}
