package com.liang.recommend.service;

import com.liang.recommend.pojo.UserCoordination2;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
public interface UserCoordination2Service extends IService<UserCoordination2> {

    /**
     * 插入距离
     * @param userId
     * @return 1成功 0失败
     */
    int toInsertUserCoordination2(Long userId);

}
