package com.liang.recommend.service;

import com.liang.recommend.pojo.Movie;
import com.liang.recommend.pojo.Rotation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-27
 */
public interface RotationService extends IService<Rotation> {

    /**
     * 得到总的推荐
     * @return
     */
    List<Rotation> getRecommend(Long userId, int index);

    /**
     * 根据最近操作的电影的id返回相似的推荐电影的集合
     * @return
     */
    List<Long> getMovieList5(Long userId);

}
