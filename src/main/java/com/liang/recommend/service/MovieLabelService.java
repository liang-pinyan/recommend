package com.liang.recommend.service;

import com.liang.recommend.pojo.MovieLabel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
public interface MovieLabelService extends IService<MovieLabel> {

}
