package com.liang.recommend.service;

import com.liang.recommend.pojo.MovieType;
import com.baomidou.mybatisplus.extension.service.IService;
import com.liang.recommend.pojo.P;

import java.math.BigInteger;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
public interface MovieTypeService extends IService<MovieType> {

    /**
     * 根据电影id获取电影类型的数组
     * @param movieId
     * @return
     */
    int[] getMovieType(int movieId);

    /**
     * 得到数据库所有数据返回一个List集合
     * @return
     */
    List<MovieType> getMovieTypeList();

    /**
     * 得到数据库中的所有数据返回一个p类型对象 （注：p对象是为了算法考虑建造的对应p矩阵的对象）
     * @return
     */
    List<P> getP();


}
