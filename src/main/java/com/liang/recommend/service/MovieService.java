package com.liang.recommend.service;

import com.liang.recommend.pojo.Movie;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
public interface MovieService extends IService<Movie> {

     /**
      * 得到总的推荐
      * @return
      */
     List<Movie> getRecommend(Long userId,int index);



     /**
      * 根据Euclidean_metric得到推荐电影列表
      * @param userId
      * @param pages
      * @return
      */
     List<Movie> getMovieList1(Long userId,int pages);

     /**
      * 根据Euclidean_metric得到推荐电影id列表
      * @param userId
      * @param pages
      * @return
      */
     List<Long> getMovieIdList1(Long userId,int pages);


     /**
      * 根据Pearson得到推荐电影列表
      * @param userId
      * @param pages
      * @return
      */
     List<Long> getMovieIdList2(Long userId,int pages);

     /**
      * 根据Latent_Factor得到推荐电影列表
      * @return
      */
     List<Long> getMovieList3(Long userId);

     /**
      * 得到随机的电影推荐列表
      * @return List<Movie>
      */
     List<Integer> getMovieList4();

     /**
      * 根据最近操作的电影的id返回相似的推荐电影的集合
      * @return
      */
     List<Long> getMovieList5(Long userId);

}
