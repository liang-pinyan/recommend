package com.liang.recommend.service;

import com.liang.recommend.pojo.UserAction;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
public interface UserActionService extends IService<UserAction> {
    /**
     * 用户行为插入
     * @param userId
     * @param movieId
     * @return
     */
    int addUserAction(int userId,int movieId);

    int updateUserAction(int userId,int movieI, float grade, float collection);

    /**
     * 得到所有用户行为的集合
     * @return
     */
    List<UserAction> getUserActionCollection();

    /**
     * 得到所有用户有过行为的电影的不重复id
     * @return
     */
    List<Long> getAllUserMovieId();

    /**
     * 得到所有有过行为的用户id
     * @return
     */
    List<Long> getAllUserId();

    /**
     * 得到真实的用户行为R矩阵
     * @return
     */
    Double[][] getRealR();


}
