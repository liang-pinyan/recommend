package com.liang.recommend.service;

import com.liang.recommend.pojo.UserCoordination;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
public interface UserCoordinationService extends IService<UserCoordination> {

    /**
     * 插入距离
     * @param userId
     * @return
     */
     int toInsertUserCoordination(Long userId);

}
