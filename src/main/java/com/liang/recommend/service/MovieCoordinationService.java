package com.liang.recommend.service;

import com.liang.recommend.pojo.Entity;
import com.liang.recommend.pojo.MovieCoordination;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
public interface MovieCoordinationService extends IService<MovieCoordination> {

    /**
     * 根据算法结果的entity对象插入电影之间的相似度
     */
    public void Insert();

}
