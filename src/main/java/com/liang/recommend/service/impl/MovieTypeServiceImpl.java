package com.liang.recommend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liang.recommend.pojo.MovieType;
import com.liang.recommend.mapper.MovieTypeMapper;
import com.liang.recommend.pojo.P;
import com.liang.recommend.service.MovieTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liang.recommend.service.UserActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Service
public class MovieTypeServiceImpl extends ServiceImpl<MovieTypeMapper, MovieType> implements MovieTypeService {
    @Autowired
    MovieTypeMapper movieTypeMapper;

    @Autowired
    UserActionService userActionService;

    @Override
    public int[] getMovieType(int movieId) {
        QueryWrapper<MovieType> wrapper = new QueryWrapper<>();
        wrapper.eq("movie_id",movieId);
        List<MovieType> types = movieTypeMapper.selectList(wrapper);
        int[] type = new int[types.size()];
        int i =0;
        for (MovieType movieType : types) {
            type[i]=movieType.getLabelId().intValue();
            i++;
        }
        return type;
    }

    @Override
    public List<MovieType> getMovieTypeList() {
        List<MovieType> list = movieTypeMapper.selectList(null);
        return list;
    }

    @Override
    public List<P> getP() {
        P p;
        QueryWrapper<MovieType> wrapper;
        List<Long> allUserMovieId = userActionService.getAllUserMovieId();
        List<P> pList = new ArrayList<>();
        Double value;
        for (Long movieId : allUserMovieId) {
            wrapper = new QueryWrapper<>();
            wrapper.eq("movie_id",movieId).orderByAsc("movie_id");
            List<MovieType> types = movieTypeMapper.selectList(wrapper);
            p = new P(movieId,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0);
            Integer size = types.size();
            for (MovieType type : types) {
                value=(double)10/size;
                p.setMethod(type.getLabelId(),value);
            }
            pList.add(p);
        }
        return pList;
    }
}
