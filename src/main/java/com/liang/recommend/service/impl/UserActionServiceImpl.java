package com.liang.recommend.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liang.recommend.pojo.UserAction;
import com.liang.recommend.mapper.UserActionMapper;
import com.liang.recommend.service.UserActionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Service
public class UserActionServiceImpl extends ServiceImpl<UserActionMapper, UserAction> implements UserActionService {
    @Autowired
    UserActionMapper userActionMapper;

    /**
     * 添加用户行为
     * @param userId
     * @param movieId
     * @return
     */
    @Override
    public int addUserAction(int userId, int movieId) {
        UserAction userAction = new UserAction();
        userAction.setUserId((long)userId);
        userAction.setMovieId((long)movieId);
        userAction.setView(1F);
        userAction.setGrade(0F);
        userAction.setCollection(0F);
        userAction.setScore(1F);
        int i = userActionMapper.insert(userAction);
        return i;
    }

    /**
     * 更新用户行为
     * @param userId
     * @param movieId
     * @param grade
     * @param collection
     * @return
     */
    @Override
    public int updateUserAction(int userId, int movieId, float grade, float collection) {
        float tempGrade=0;
        float tempCollection=0;
        float score=0;
        if (grade!=0){
            tempGrade=grade;
        }
        if (collection!=0){
            tempCollection=collection;
        }
        score= 1 + ((2*tempGrade)/5) + tempCollection;
        System.out.println(score);//调试效果
        UserAction userAction = new UserAction();
        userAction.setUserId((long)userId);
        userAction.setMovieId((long)movieId);
        userAction.setGrade(tempGrade);
        userAction.setCollection(tempCollection);
        userAction.setScore(score);
        QueryWrapper<UserAction> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",userId).eq("movie_id",movieId);
        userActionMapper.update(userAction,wrapper);
        int i = userActionMapper.update(userAction,wrapper);;
        return i;
    }


    @Override
    public List<UserAction> getUserActionCollection() {
        List<UserAction> userActions = userActionMapper.selectList(null);
        return userActions;
    }

    @Override
    public List<Long> getAllUserMovieId() {
        QueryWrapper<UserAction> wrapper = new QueryWrapper<>();
        wrapper.select("DISTINCT movie_id").orderByAsc("movie_id");
        List<UserAction> userActions = userActionMapper.selectList(wrapper);
        List<Long> list = new ArrayList<Long>();
        for (UserAction userAction : userActions) {
            list.add(userAction.getMovieId());
        }
        return list;
    }

    @Override
    public List<Long> getAllUserId() {
        QueryWrapper<UserAction> wrapper = new QueryWrapper<>();
        wrapper.select("DISTINCT user_id").orderByAsc("user_id");
        List<UserAction> userActions = userActionMapper.selectList(wrapper);
        List<Long> list = new ArrayList<Long>();
        for (UserAction userAction : userActions) {
            list.add(userAction.getUserId());
        }
        return list;
    }

    @Override
    public Double[][] getRealR() {
        List<UserAction> userActions = userActionMapper.selectList(null);
        HashSet<Long> users = new HashSet<>();
        HashSet<Long> movies = new HashSet<>();
        HashMap<Long, Float> map = new HashMap<>();
        for (UserAction userAction : userActions) {
            users.add(userAction.getUserId());
            movies.add(userAction.getMovieId());
        }
        Double[][] arr = new Double[users.size()][movies.size()];
        for (UserAction userAction : userActions) {
            arr[userAction.getUserId().intValue()-1][userAction.getMovieId().intValue()-1]=userAction.getScore().doubleValue();
        }
        //填充防止空指针
        for (Double[] doubles : arr) {
            for (Double aDouble : doubles) {
                if (aDouble==null){
                    aDouble=0.0;
                }
            }
        }
        return arr;
    }

}
