package com.liang.recommend.service.impl;

import com.liang.recommend.pojo.MovieLabel;
import com.liang.recommend.mapper.MovieLabelMapper;
import com.liang.recommend.service.MovieLabelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Service
public class MovieLabelServiceImpl extends ServiceImpl<MovieLabelMapper, MovieLabel> implements MovieLabelService {

}
