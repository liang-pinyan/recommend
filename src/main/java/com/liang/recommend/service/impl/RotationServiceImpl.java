package com.liang.recommend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liang.recommend.algorithm.RandomRecommend;
import com.liang.recommend.algorithm.Vote;
import com.liang.recommend.mapper.MovieCoordinationMapper;
import com.liang.recommend.pojo.Movie;
import com.liang.recommend.pojo.MovieCoordination;
import com.liang.recommend.pojo.Rotation;
import com.liang.recommend.mapper.RotationMapper;
import com.liang.recommend.recommend.BashRecommend;
import com.liang.recommend.service.MovieService;
import com.liang.recommend.service.RotationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liang.recommend.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-27
 */
@Service
public class RotationServiceImpl extends ServiceImpl<RotationMapper, Rotation> implements RotationService {
    @Autowired
    MovieService movieService;
    @Autowired
    Vote vote;
    @Autowired
    RotationMapper rotationMapper;
    @Autowired
    MovieCoordinationMapper movieCoordinationMapper;
    @Autowired
    BashRecommend bashRecommend;
    @Autowired
    RedisUtil redisUtil;

    private List<Rotation> temp; //提高作用域

    @Override
    public List<Rotation> getRecommend(Long userId, int index) {
        int pages=1;//对应向下取邻居
        List<Long> result;
        List<Long> get;
        List<Rotation> movies=new ArrayList<Rotation>();
        List<Long> A = movieService.getMovieIdList1(userId, pages);
        List<Long> B = movieService.getMovieList3(userId);
        List<Long> C = movieService.getMovieIdList2(userId, pages);
        result = vote.voteResult(A, B, C);
        get=result.subList((index-1)*5,(index-1)*5+5);
        QueryWrapper<Rotation> wrapper = new QueryWrapper<>();
        wrapper.in("movie_id",get);
        temp = rotationMapper.selectList(wrapper);
        if (get.size()<5){
            index=1;
            pages+=1; //使邻居向下
            movies.add(temp.get(0));
            A = movieService.getMovieIdList1(userId, pages);
            C = movieService.getMovieIdList2(userId, pages);
            result = vote.voteResult(A, B, C);
            get=result.subList((index-1)*5,(index-1)*5+4);
            QueryWrapper<Rotation> wrapper2 = new QueryWrapper<>();
            wrapper2.in("movie_id",get);
            this.temp = rotationMapper.selectList(wrapper2);//更新temp
            if (temp.size()<5){ //向下邻居都不够时
                RandomRecommend randomRecommend = new RandomRecommend();
                List<Integer> random = randomRecommend.getRandom();
                QueryWrapper<Rotation> wrapper1 = new QueryWrapper<>();
                wrapper1.in("movie_id",random);
                List<Rotation> list = rotationMapper.selectList(wrapper1);
                temp.addAll(list);
                temp.subList(0,5);
            }
        }
        for (Rotation rotation : temp) {
            movies.add(rotation);
        }
        return movies;
    }

    @Override
    public List<Long> getMovieList5(Long userId) {
        String key = Long.toString(userId);
        List<Object> objects = redisUtil.lGet(key, 0, -1);
        List<Long> list = new ArrayList<>();
        for (Object object : objects) {
            list.add(Long.valueOf(String.valueOf(object)));
        }
        QueryWrapper<MovieCoordination> wrapper = new QueryWrapper<>();
        wrapper
                .select("distinct movie_id2")
                .in("movie_id1",list).orderByDesc("distance");
        List<MovieCoordination> moviesList = movieCoordinationMapper.selectList(wrapper);
        List<Long> movies = new ArrayList<>();
        for (MovieCoordination coordination : moviesList) {
            movies.add(coordination.getMovieId2());
        }
        return bashRecommend.getRecommendMovieIdList(userId,movies); //去重
    }

}
