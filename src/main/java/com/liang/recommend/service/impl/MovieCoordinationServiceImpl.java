package com.liang.recommend.service.impl;

import com.liang.recommend.algorithm.Item;
import com.liang.recommend.pojo.Entity;
import com.liang.recommend.pojo.MovieCoordination;
import com.liang.recommend.mapper.MovieCoordinationMapper;
import com.liang.recommend.service.MovieCoordinationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liang.recommend.service.UserActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Service
public class MovieCoordinationServiceImpl extends ServiceImpl<MovieCoordinationMapper, MovieCoordination> implements MovieCoordinationService {

    @Autowired
    MovieCoordinationMapper movieCoordinationMapper;
    @Autowired
    UserActionService userActionService;


    @Override
    public void Insert() {
        Item item = new Item();
        Map<Long, List<Long>> map = item.stepOne(userActionService.getAllUserId(), userActionService.getUserActionCollection());
        Entity entity = item.stepTwo(map, userActionService.getAllUserId(), userActionService.getAllUserMovieId());
        List<Long> movieId = entity.getMovieId();
        Integer[][] matrix = entity.getMatrix();

        if (movieId.size()==matrix[0].length){
            HashMap<Long, Integer> map1 = new HashMap<>();
            int i=0;
            for (Long id : movieId) {
                map1.put(id,i);
                i+=1;
            }
            HashMap<Integer, Long> map2 = new HashMap<>();
            int j=0;
            for (Long aLong : movieId) {
                map2.put(j,aLong);
                j+=1;
            }
            for (Long id : movieId) {
                int flag=0;
                for (Integer integer : matrix[map1.get(id)]) {
                    if (integer!=0){
                        MovieCoordination movieCoordination = new MovieCoordination();
                        movieCoordination.setMovieId1(id);
                        movieCoordination.setMovieId2(map2.get(flag));
                        movieCoordination.setDistance(integer);
                        movieCoordinationMapper.insert(movieCoordination);
                    }
                    flag+=1;
                }
            }

        }else {
            System.out.println("参数之间不符合要求，请检查参数");
        }

    }
}
