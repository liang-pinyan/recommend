package com.liang.recommend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liang.recommend.algorithm.Euclidean_metric;
import com.liang.recommend.mapper.UserLikeMapper;
import com.liang.recommend.pojo.UserCoordination;
import com.liang.recommend.mapper.UserCoordinationMapper;
import com.liang.recommend.pojo.UserLike;
import com.liang.recommend.service.UserCoordinationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Service
public class UserCoordinationServiceImpl extends ServiceImpl<UserCoordinationMapper, UserCoordination> implements UserCoordinationService {

    @Autowired
    UserLikeMapper userLikeMapper;
    @Autowired
    UserCoordinationMapper userCoordinationMapper;

    @Override
    public int toInsertUserCoordination(Long userId) {
        Long temp;
        double distance=0;
        Euclidean_metric euclidean = new Euclidean_metric();
        UserLike userLike = userLikeMapper.selectById(userId);
        List<UserLike> userLikes = userLikeMapper.selectList(null);
        for (UserLike like : userLikes) {
            Long userId2 = like.getUserId();
            if (userId2 != userId) {
                temp=userId2;
                distance = euclidean.euclidean_dis(userLike, like);
                UserCoordination userCoordination = new UserCoordination();
                userCoordination.setUserId1(userId);
                userCoordination.setUserId2(temp);
                userCoordination.setDistance(distance);
                userCoordinationMapper.insert(userCoordination);
            }
        }
        return 0;
    }
}
