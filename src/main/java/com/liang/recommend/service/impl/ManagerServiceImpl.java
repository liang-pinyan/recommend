package com.liang.recommend.service.impl;

import com.liang.recommend.mapper.*;
import com.liang.recommend.pojo.UserLike;
import com.liang.recommend.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    UserCoordinationMapper userCoordinationMapper;
    @Autowired
    UserCoordination2Mapper userCoordination2Mapper;
    @Autowired
    LatentFactorMapper latentFactorMapper;
    @Autowired
    UserActionMapper userActionMapper;
    @Autowired
    UserLikeMapper userLikeMapper;
    @Autowired
    MovieCoordinationMapper movieCoordinationMapper;


    @Override
    public void delete1() {
        userCoordinationMapper.deleteAll();
    }

    public void delete2() {
        userCoordination2Mapper.deleteAll();
    }

    @Override
    public void delete3() {
        latentFactorMapper.deleteAll();
    }

    @Override
    public void deleteActionData() {
        userActionMapper.deleteAll();
        userLikeMapper.deleteAll();
    }

    @Override
    public void deleteMovieCoordination() {
        movieCoordinationMapper.deleteAll();
    }

}
