package com.liang.recommend.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liang.recommend.mapper.UserActionMapper;
import com.liang.recommend.pojo.UserLike;
import com.liang.recommend.mapper.UserLikeMapper;
import com.liang.recommend.service.MovieTypeService;
import com.liang.recommend.service.UserLikeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Service
public class UserLikeServiceImpl extends ServiceImpl<UserLikeMapper, UserLike> implements UserLikeService {
    @Autowired
    UserLikeMapper userLikeMapper;
    @Autowired
    UserActionMapper userActionMapper;
    @Autowired
    MovieTypeService movieTypeService;

    /**
     * 插入一条潜在因子记录到userLike
     * @param userId
     * @return 1成功 0失败
     */
    @Override
    public int addUser(int userId) {
        UserLike userLike = new UserLike((long)userId,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F,0F);
        int i= userLikeMapper.insert(userLike);
        return i;
    }

    /**
     * 更新潜在因子userLike表
     * @param userId
     * @param movieId
     * @return 1成功 0失败
     */
    @Override
    public int updateUser(Integer userId,Integer movieId,Float grade,Float collection) {
        Double score;
        float value;
        int[] movieType = movieTypeService.getMovieType(movieId); //得到该电影标签集合
        UserLike userLike = userLikeMapper.selectById(userId);//得到表中原本的信息作为蓝图
        if (movieType.length==0){
            return 0;
        }else {
            for (int v : movieType) { //遍历标签
                score = userLike.getMethod(v)+(1 +((2*grade)/5)+collection)*1.0; //score=sum+1.0*行为分
                value = score.floatValue();
                userLike.setMethod(v,value);
            }
            QueryWrapper<UserLike> wrapper = new QueryWrapper<>();
            wrapper.eq("user_id",userId);
            int i = userLikeMapper.update(userLike,wrapper);
            return i;
        }
    }

    /**
     * 得到所有的UserLike信息
     * @return List<UserLike>
     */
    @Override
    public List<UserLike> getUserLikeList() {
        List<UserLike> userLikes = userLikeMapper.selectList(new QueryWrapper<UserLike>().orderByAsc("user_id"));
        return userLikes;
    }
}
