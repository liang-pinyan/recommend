package com.liang.recommend.service;

import com.liang.recommend.pojo.LatentFactor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-25
 */
public interface LatentFactorService extends IService<LatentFactor> {


    /**
     * 插入矩阵R
     * @return 1成功 0失败
     */
    int toInsertLatentFactor(Double[][] matrixR,Long[] axisQ,Long[] axisP);

}
