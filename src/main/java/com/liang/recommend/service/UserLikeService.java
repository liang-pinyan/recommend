package com.liang.recommend.service;

import com.liang.recommend.pojo.UserLike;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigInteger;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
public interface UserLikeService extends IService<UserLike> {

    /**
     * 注册时开辟一个userlike记录
     * @param userId
     * @return
     */
    int addUser(int userId);

    /**
     * 更新userlike表方法
     * @param userId
     * @return
     */
    int updateUser(Integer userId,Integer movieId,Float grade,Float collection);

    /**
     * 得到所有用户行为的集合
     * @return
     */
    List<UserLike> getUserLikeList();





}
