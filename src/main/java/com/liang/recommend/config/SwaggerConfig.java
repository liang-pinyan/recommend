package com.liang.recommend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private ApiInfo apiInfo(){
        Contact contact = new Contact("小梁同学", "#", "xiaoliangworking@163.com");

        return new ApiInfo("小梁的接口日志",
                "api日志",
                "1.0",
                "127.0.0.1",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }

    @Bean
    //配置swagger的docket的bean实例
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
//              .enable(false)//启动开关
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.liang.recommend.controller"))//配置要扫描接口的方式
                .build();//
    }
}
