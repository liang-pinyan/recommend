package com.liang.recommend.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserCoordination2对象", description="")
public class UserCoordination2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "唯一Id")
    @TableId(value = "c_id", type = IdType.AUTO)
    private Long cId;

    @ApiModelProperty(value = "用户Id")
    private Long userId1;

    @ApiModelProperty(value = "用户Id")
    private Long userId2;

    private Double distance;

    @ApiModelProperty(value = "相关型（0为正相关，1为负相关）")
    private Long type;

}
