package com.liang.recommend.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="MovieCoordination对象", description="")
public class MovieCoordination implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "唯一Id")
    @TableId(value = "c_id", type = IdType.AUTO)
    private Long cId;

    @ApiModelProperty(value = "电影Id")
    private Long movieId1;

    @ApiModelProperty(value = "电影Id")
    private Long movieId2;

    private Integer distance;


}
