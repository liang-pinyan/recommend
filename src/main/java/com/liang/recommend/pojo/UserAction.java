package com.liang.recommend.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 小梁
 * @since 2020-12-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserAction对象", description="")
public class UserAction implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "唯一Id")
    @TableId(value = "a_id", type = IdType.AUTO)
    private Long aId;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    private Long movieId;

    @ApiModelProperty(value = "浏览行为=1")
    private Float view;

    @ApiModelProperty(value = "评分=2")
    private Float grade;

    @ApiModelProperty(value = "收藏行为=3")
    private Float collection;

    @ApiModelProperty(value = "行为得分")
    private Float score;


}
