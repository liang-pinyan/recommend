package com.liang.recommend.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 小梁
 * @since 2020-12-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="LatentFactor对象", description="")
public class LatentFactor implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "唯一id")
    @TableId(value = "a_id", type = IdType.AUTO)
    private Long aId;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "电影id")
    private Long movieId;

    @ApiModelProperty(value = "潜在因子分数")
    private Double value;


}
