package com.liang.recommend.pojo;

import lombok.Data;


@Data
public class Action {

    private int userId;
    private int movieId;
    private Float grade;
    private Float collection;

}
