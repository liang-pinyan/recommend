package com.liang.recommend.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Entity implements Serializable {
    private List<Long> movieId;
    private Integer[][] matrix;
}
