package com.liang.recommend.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieVO implements Serializable {

    private String pictureName;
    private double score;
    private Long mId;
    private String mName;
}
