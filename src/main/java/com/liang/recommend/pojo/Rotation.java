package com.liang.recommend.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author xiao
 * @since 2020-12-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Rotation对象", description="")
public class Rotation implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "movie_id", type = IdType.AUTO)
    private Long movieId;

    private String rotationChart;
}