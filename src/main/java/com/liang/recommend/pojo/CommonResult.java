package com.liang.recommend.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author son
 * @create 2020-12-01 14:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResult<T>{

    private Integer code;
    private Boolean success;
    private T data;

    public CommonResult(Integer code, Boolean success){
        this(code,success,null);
    }
}

