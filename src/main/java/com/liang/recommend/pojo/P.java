package com.liang.recommend.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class P implements Serializable {

    @ApiModelProperty(value = "电影id")
    private Long movieId;

    @ApiModelProperty(value = "剧情类型")
    private Double typePlotValue;

    @ApiModelProperty(value = "喜剧类型")
    private Double typeComedyValue;

    @ApiModelProperty(value = "动作类型")
    private Double typeActionValue;

    @ApiModelProperty(value = "爱情类型")
    private Double typeLoveValue;

    @ApiModelProperty(value = "科幻类型")
    private Double typeScienceValue;

    @ApiModelProperty(value = "动画类型")
    private Double typeCartoonValue;

    @ApiModelProperty(value = "悬疑类型")
    private Double typeSuspenseValue;

    @ApiModelProperty(value = "惊悚类型")
    private Double typeHorrorValue;

    @ApiModelProperty(value = "恐怖类型")
    private Double typeTerrorValue;

    @ApiModelProperty(value = "犯罪类型")
    private Double typeCrimeValue;

    @ApiModelProperty(value = "同性类型")
    private Double typeGayValue;

    @ApiModelProperty(value = "音乐类型")
    private Double typeMusicValue;

    @ApiModelProperty(value = "歌舞类型")
    private Double typeSgValue;

    @ApiModelProperty(value = "传记类型")
    private Double typeBiographicalValue;

    @ApiModelProperty(value = "历史类型")
    private Double typeHistoricalValue;

    @ApiModelProperty(value = "战争类型")
    private Double typeWarValue;

    @ApiModelProperty(value = "西部类型")
    private Double typeWesternValue;

    @ApiModelProperty(value = "奇幻类型")
    private Double typeFantasyValue;

    @ApiModelProperty(value = "冒险类型")
    private Double typeAdventureValue;

    @ApiModelProperty(value = "灾难类型")
    private Double typeDisasterValue;

    @ApiModelProperty(value = "武侠类型")
    private Double typeMartialValue;

    public Double getMethod(int i){
        if (i==1){
            return getTypePlotValue();
        }
        if (i==2){
            return getTypeComedyValue();
        }
        if (i==3){
            return getTypeActionValue();
        }
        if (i==4){
            return getTypeLoveValue();
        }
        if (i==5){
            return getTypeScienceValue();
        }
        if (i==6){
            return getTypeCartoonValue();
        }
        if (i==7){
            return getTypeSuspenseValue();
        }
        if (i==8){
            return getTypeHorrorValue();
        }
        if (i==9){
            return getTypeTerrorValue();
        }
        if (i==10){
            return getTypeCrimeValue();
        }
        if (i==11){
            return getTypeGayValue();
        }
        if (i==12){
            return getTypeMusicValue();
        }
        if (i==13){
            return getTypeSgValue();
        }
        if (i==14){
            return getTypeBiographicalValue();
        }
        if (i==15){
            return getTypeHistoricalValue();
        }
        if (i==16){
            return getTypeWarValue();
        }
        if (i==17){
            return getTypeWesternValue();
        }
        if (i==18){
            return getTypeFantasyValue();
        }
        if (i==19){
            return getTypeAdventureValue();
        }
        if (i==20){
            return getTypeDisasterValue();
        }
        if (i==21){
            return getTypeMartialValue();
        }
        return 0.0;
    }

    public P setMethod( Long i,Double value) {
        if (i == 1) {
            this.setTypePlotValue(value);
            return this;
        }
        if (i == 2) {
            this.setTypeComedyValue(value);
            return this;
        }
        if (i == 3) {
            this.setTypeActionValue(value);
            return this;

        }
        if (i == 4) {
            this.setTypeLoveValue(value);
            return this;
        }
        if (i == 5) {
            this.setTypeScienceValue(value);
            return this;
        }
        if (i == 6) {
            this.setTypeCartoonValue(value);
            return this;
        }
        if (i == 7) {
            this.setTypeSuspenseValue(value);
            return this;
        }
        if (i == 8) {
            this.setTypeHorrorValue(value);
            return this;
        }
        if (i == 9) {
            this.setTypeTerrorValue(value);
            return this;
        }
        if (i == 10) {
            this.setTypeCrimeValue(value);
            return this;
        }
        if (i == 11) {
            this.setTypeGayValue(value);
            return this;
        }
        if (i == 12) {
            this.setTypeMusicValue(value);
            return this;
        }
        if (i == 13) {
            this.setTypeSgValue(value);
            return this;
        }
        if (i == 14) {
            this.setTypeBiographicalValue(value);
            return this;
        }
        if (i == 15) {
            this.setTypeHistoricalValue(value);
            return this;
        }
        if (i == 16) {
            this.setTypeWarValue(value);
            return this;
        }
        if (i == 17) {
            this.setTypeWesternValue(value);
            return this;
        }
        if (i == 18) {
            this.setTypeFantasyValue(value);
            return this;
        }
        if (i == 19) {
            this.setTypeAdventureValue(value);
            return this;
        }
        if (i == 20) {
            this.setTypeDisasterValue(value);
            return this;
        }
        if (i == 21) {
            this.setTypeMartialValue(value);
            return this;
        }
        return this;
    }


}
